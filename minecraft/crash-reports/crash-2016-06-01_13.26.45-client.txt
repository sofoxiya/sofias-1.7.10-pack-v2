---- Minecraft Crash Report ----
// I bet Cylons wouldn't have this problem.

Time: 01/06/16 13:26
Description: There was a severe problem during mod loading that has caused the game to fail

cpw.mods.fml.common.LoaderException: java.lang.NoClassDefFoundError: mods/immibis/core/impl/crossmod/EnergyConsumerTraitImpl_IC2
	at cpw.mods.fml.common.LoadController.transition(LoadController.java:163)
	at cpw.mods.fml.common.Loader.loadMods(Loader.java:544)
	at cpw.mods.fml.client.FMLClientHandler.beginMinecraftLoading(FMLClientHandler.java:208)
	at net.minecraft.client.Minecraft.func_71384_a(Minecraft.java:480)
	at net.minecraft.client.Minecraft.func_99999_d(Minecraft.java:878)
	at net.minecraft.client.main.Main.main(SourceFile:148)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at org.multimc.onesix.OneSixLauncher.launchWithMainClass(OneSixLauncher.java:282)
	at org.multimc.onesix.OneSixLauncher.launch(OneSixLauncher.java:364)
	at org.multimc.EntryPoint.listen(EntryPoint.java:170)
	at org.multimc.EntryPoint.main(EntryPoint.java:54)
Caused by: java.lang.NoClassDefFoundError: mods/immibis/core/impl/crossmod/EnergyConsumerTraitImpl_IC2
	at mods.immibis.core.ImmibisCore.initPreferredEnergySystem(ImmibisCore.java:176)
	at mods.immibis.core.ImmibisCore.<init>(ImmibisCore.java:114)
	at mods.immibis.core.ICNonCoreMod.<init>(ICNonCoreMod.java:24)
	at sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
	at sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)
	at sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)
	at java.lang.reflect.Constructor.newInstance(Unknown Source)
	at java.lang.Class.newInstance(Unknown Source)
	at cpw.mods.fml.common.ILanguageAdapter$JavaAdapter.getNewInstance(ILanguageAdapter.java:173)
	at cpw.mods.fml.common.FMLModContainer.constructMod(FMLModContainer.java:506)
	at sun.reflect.GeneratedMethodAccessor4.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at com.google.common.eventbus.EventSubscriber.handleEvent(EventSubscriber.java:74)
	at com.google.common.eventbus.SynchronizedEventSubscriber.handleEvent(SynchronizedEventSubscriber.java:47)
	at com.google.common.eventbus.EventBus.dispatch(EventBus.java:322)
	at com.google.common.eventbus.EventBus.dispatchQueuedEvents(EventBus.java:304)
	at com.google.common.eventbus.EventBus.post(EventBus.java:275)
	at cpw.mods.fml.common.LoadController.sendEventToModContainer(LoadController.java:212)
	at cpw.mods.fml.common.LoadController.propogateStateMessage(LoadController.java:190)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at com.google.common.eventbus.EventSubscriber.handleEvent(EventSubscriber.java:74)
	at com.google.common.eventbus.SynchronizedEventSubscriber.handleEvent(SynchronizedEventSubscriber.java:47)
	at com.google.common.eventbus.EventBus.dispatch(EventBus.java:322)
	at com.google.common.eventbus.EventBus.dispatchQueuedEvents(EventBus.java:304)
	at com.google.common.eventbus.EventBus.post(EventBus.java:275)
	at cpw.mods.fml.common.LoadController.distributeStateMessage(LoadController.java:119)
	at cpw.mods.fml.common.Loader.loadMods(Loader.java:513)
	... 18 more
Caused by: java.lang.ClassNotFoundException: mods.immibis.core.impl.crossmod.EnergyConsumerTraitImpl_IC2
	at net.minecraft.launchwrapper.LaunchClassLoader.findClass(LaunchClassLoader.java:191)
	at java.lang.ClassLoader.loadClass(Unknown Source)
	at java.lang.ClassLoader.loadClass(Unknown Source)
	... 49 more
Caused by: java.lang.NoClassDefFoundError: ic2/api/energy/tile/IEnergySink
	at java.lang.ClassLoader.defineClass1(Native Method)
	at java.lang.ClassLoader.defineClass(Unknown Source)
	at java.security.SecureClassLoader.defineClass(Unknown Source)
	at net.minecraft.launchwrapper.LaunchClassLoader.findClass(LaunchClassLoader.java:182)
	... 51 more
Caused by: java.lang.ClassNotFoundException: ic2.api.energy.tile.IEnergySink
	at net.minecraft.launchwrapper.LaunchClassLoader.findClass(LaunchClassLoader.java:101)
	at java.lang.ClassLoader.loadClass(Unknown Source)
	at java.lang.ClassLoader.loadClass(Unknown Source)
	... 55 more


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
Details:
	Minecraft Version: 1.7.10
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_91, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 1624887320 bytes (1549 MB) / 1898446848 bytes (1810 MB) up to 3817865216 bytes (3641 MB)
	JVM Flags: 3 total; -XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -Xms512m -Xmx4096m
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: Minecraft 1.7.10 MCP v9.05 FML v7.10.99.99 Minecraft Forge 10.13.4.1614 Optifine OptiFine_1.7.10_HD_U_D4 104 mods loaded, 104 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	UC	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	UC	FML{7.10.99.99} [Forge Mod Loader] (forge-1.7.10-10.13.4.1614-1.7.10-universal.jar) 
	UC	Forge{10.13.4.1614} [Minecraft Forge] (forge-1.7.10-10.13.4.1614-1.7.10-universal.jar) 
	UC	appliedenergistics2-core{rv2-stable-10} [AppliedEnergistics2 Core] (minecraft.jar) 
	UC	CodeChickenCore{1.0.7.47} [CodeChicken Core] (minecraft.jar) 
	UC	Covers1624Core{1.7.10-1.0} [Covers1624 Core] (minecraft.jar) 
	UC	NotEnoughItems{1.0.5.120} [Not Enough Items] (NotEnoughItems-1.7.10-1.0.5.120-universal.jar) 
	UC	OpenComputers|Core{1.6.0.3-rc.1} [OpenComputers (Core)] (minecraft.jar) 
	UC	OpenEye{0.6} [OpenEye] (OpenEye-0.6-1.7.10.jar) 
	UC	OpenModsCore{0.9.1} [OpenModsCore] (minecraft.jar) 
	UC	<CoFH ASM>{000} [CoFH ASM] (minecraft.jar) 
	UC	<DragonAPI ASM>{0} [DragonAPI ASM Data Initialization] (minecraft.jar) 
	UC	FastCraft{1.23} [FastCraft] (fastcraft-1.23.jar) 
	UC	bspkrsCore{6.16} [bspkrsCore] ([1.7.10]bspkrsCore-universal-6.16.jar) 
	UC	ArmorStatusHUD{1.28} [ArmorStatusHUD] ([1.7.10]ArmorStatusHUD-client-1.28.jar) 
	UC	StartingInventory{1.7.10.r03} [StartingInventory] ([1.7.10]StartingInventory-universal-1.7.10.r03.jar) 
	UC	Treecapitator{1.7.10} [Treecapitator] ([1.7.10]Treecapitator-universal-2.0.4.jar) 
	UC	appliedenergistics2{rv2-stable-10} [Applied Energistics 2] (appliedenergistics2-rv2-stable-10.jar) 
	UC	CoFHCore{1.7.10R3.1.2} [CoFH Core] (CoFHCore-[1.7.10]3.1.2-325.jar) 
	UC	asielib{0.4.5} [asielib] (AsieLib-1.7.10-0.4.5.jar) 
	UC	BiblioCraft{1.11.4} [BiblioCraft] (BiblioCraft[v1.11.4][MC1.7.10].jar) 
	UC	Forestry{4.2.12.60} [Forestry for Minecraft] (forestry_1.7.10-4.2.12.60.jar) 
	UC	BiblioWoodsForestry{1.7} [BiblioWoods Forestry Edition] (BiblioWoods[Forestry][v1.7].jar) 
	UC	BuildCraft|Core{7.1.16} [BuildCraft] (buildcraft-7.1.16.jar) 
	UC	BuildCraft|Builders{7.1.16} [BC Builders] (buildcraft-7.1.16.jar) 
	UC	BuildCraft|Transport{7.1.16} [BC Transport] (buildcraft-7.1.16.jar) 
	UC	BuildCraft|Energy{7.1.16} [BC Energy] (buildcraft-7.1.16.jar) 
	UC	BuildCraft|Silicon{7.1.16} [BC Silicon] (buildcraft-7.1.16.jar) 
	UC	BuildCraft|Robotics{7.1.16} [BC Robotics] (buildcraft-7.1.16.jar) 
	UC	BuildCraft|Factory{7.1.16} [BC Factory] (buildcraft-7.1.16.jar) 
	UC	ThermalFoundation{1.7.10R1.2.3} [Thermal Foundation] (ThermalFoundation-[1.7.10]1.2.3-112.jar) 
	UC	ThermalExpansion{1.7.10R4.1.2} [Thermal Expansion] (ThermalExpansion-[1.7.10]4.1.2-240.jar) 
	UC	BuildCraft|Compat{7.1.5} [BuildCraft Compat] (buildcraft-compat-7.1.5.jar) 
	UC	CarpentersBlocks{3.3.7} [Carpenter's Blocks] (Carpenter's Blocks v3.3.7 - MC 1.7.10.jar) 
	UC	ChickenChunks{1.3.4.19} [ChickenChunks] (ChickenChunks-1.7.10-1.3.4.19-universal.jar) 
	UC	climatecontrol{0.4} [Climate Control] (ClimateControl-0.5.beta53.jar) 
	UC	EnderStorage{1.4.7.38} [EnderStorage] (EnderStorage-1.7.10-1.4.7.38-universal.jar) 
	UC	ForgeMultipart{1.2.0.347} [Forge Multipart] (ForgeMultipart-1.7.10-1.2.0.347-universal.jar) 
	UC	MrTJPCoreMod{1.1.0.33} [MrTJPCore] (MrTJPCore-1.7.10-1.1.0.33-universal.jar) 
	UC	ProjRed|Core{4.7.0pre12.95} [ProjectRed Core] (ProjectRed-1.7.10-4.7.0pre12.95-Base.jar) 
	UC	ProjRed|Transmission{4.7.0pre12.95} [ProjectRed Transmission] (ProjectRed-1.7.10-4.7.0pre12.95-Integration.jar) 
	UC	OpenComputers{1.6.0.3-rc.1} [OpenComputers] (OpenComputers-MC1.7.10-1.6.0.3-rc.1-universal.jar) 
	UC	Waila{1.5.10} [Waila] (Waila-1.5.10_1.7.10.jar) 
	UC	computronics{1.6.1} [Computronics] (Computronics-1.7.10-1.6.1-ermahgurd-sine-wavez-OC1.6.jar) 
	UC	DamageIndicatorsMod{3.2.0} [Damage Indicators] (Damage-Indicators-Mod-1.7.10.jar) 
	UC	DragonAPI{1.0} [DragonAPI] (DragonAPI 1.7.10 V13b.jar) 
	UC	DynamicLights{1.3.9} [Dynamic Lights] (DynamicLights-1.7.10.jar) 
	UC	DynamicLights_onFire{1.0.5} [Dynamic Lights Burning Entity Module] (DynamicLights-1.7.10.jar) 
	UC	DynamicLights_creepers{1.0.4} [Dynamic Lights Creeper Module] (DynamicLights-1.7.10.jar) 
	UC	DynamicLights_dropItems{1.0.8} [Dynamic Lights EntityItem Module] (DynamicLights-1.7.10.jar) 
	UC	DynamicLights_entityClasses{1.0.1} [Dynamic Lights Entity Light Module] (DynamicLights-1.7.10.jar) 
	UC	DynamicLights_mobEquipment{1.0.8} [Dynamic Lights Mob Equipment Light Module] (DynamicLights-1.7.10.jar) 
	UC	DynamicLights_flameArrows{1.0.0} [Dynamic Lights Fiery Arrows Light Module] (DynamicLights-1.7.10.jar) 
	UC	DynamicLights_floodLights{1.0.2} [Dynamic Lights Flood Light] (DynamicLights-1.7.10.jar) 
	UC	DynamicLights_otherPlayers{1.0.8} [Dynamic Lights OtherPlayers Light Module] (DynamicLights-1.7.10.jar) 
	UC	DynamicLights_thePlayer{1.1.4} [Dynamic Lights Player Light Module] (DynamicLights-1.7.10.jar) 
	UC	eplus{3.0.2-d} [Enchanting Plus] (EnchantingPlus-1.7.10-3.0.2-d.jar) 
	UC	enhancedportals{3.0.12} [EnhancedPortals] (EnhancedPortals_1.7.10-universal-3.0.12.jar) 
	UC	etfuturum{1.5.5} [Et Futurum] (Et Futurum-1.5.5.jar) 
	UC	rftl{1.7.10} [Rotten Flesh to Leather] (Forge SSP SMP - Rotten Flesh 1.7.2 1.7.10.jar) 
	UC	foxlib{1.7.10-0.7.0} [FoxLib] (FoxLib-1.7.10-0.7.0.jar) 
	UE	ImmibisCore{59.1.4} [Immibis Core] (immibis-core-59.1.4.jar) 
	UC	inventorytweaks{1.59-dev-152-cf6e263} [Inventory Tweaks] (InventoryTweaks-1.59-dev-152.jar) 
	UC	IronChest{6.0.62.742} [Iron Chest] (ironchest-1.7.10-6.0.60.741-universal.jar) 
	UC	JABBA{1.2.1} [JABBA] (Jabba-1.2.1a_1.7.10.jar) 
	UC	journeymap{5.1.4p1} [JourneyMap] (journeymap-1.7.10-5.1.4p1-unlimited.jar) 
	UC	LogisticsPipes{0.9.3.119} [Logistics Pipes] (logisticspipes-0.9.3.119.jar) 
	UC	malisiscore{1.7.10-0.14.1} [MalisisCore] (malisiscore-1.7.10-0.14.1.jar) 
	UC	malisisdoors{1.7.10-1.13.0} [Malisis' Doors] (malisisdoors-1.7.10-1.13.0.jar) 
	UC	NEIAddons{1.12.14.40} [NEI Addons] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UC	NEIAddons|Developer{1.12.14.40} [NEI Addons: Developer Tools] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UC	NEIAddons|AppEng{1.12.14.40} [NEI Addons: Applied Energistics 2] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UC	NEIAddons|Botany{1.12.14.40} [NEI Addons: Botany] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UC	NEIAddons|Forestry{1.12.14.40} [NEI Addons: Forestry] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UC	NEIAddons|CraftingTables{1.12.14.40} [NEI Addons: Crafting Tables] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UC	NEIAddons|ExNihilo{1.12.14.40} [NEI Addons: Ex Nihilo] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UC	neiintegration{1.1.2} [NEI Integration] (NEIIntegration-MC1.7.10-1.1.2.jar) 
	UC	netherportalfix{1.0} [Nether Portal Fix] (netherportalfix-mc1.7.10-1.1.0.jar) 
	UC	numina{0.4.0.131} [Numina] (Numina-0.4.0.131.jar) 
	UC	OpenMods{0.9.1} [OpenMods] (OpenModsLib-1.7.10-0.9.1.jar) 
	UC	OpenBlocks{1.5.1} [OpenBlocks] (OpenBlocks-1.7.10-1.5.1.jar) 
	UC	OpenPeripheralCore{1.3} [OpenPeripheralCore] (OpenPeripheral-1.7.10-AIO-7.jar) 
	UC	OpenPeripheral{0.5.1} [OpenPeripheralAddons] (OpenPeripheral-1.7.10-AIO-7.jar) 
	UC	OpenPeripheralIntegration{0.5} [OpenPeripheralIntegration] (OpenPeripheral-1.7.10-AIO-7.jar) 
	UC	ProjRed|Compatibility{4.7.0pre12.95} [ProjectRed Compatibility] (ProjectRed-1.7.10-4.7.0pre12.95-Compat.jar) 
	UC	ProjRed|Integration{4.7.0pre12.95} [ProjectRed Integration] (ProjectRed-1.7.10-4.7.0pre12.95-Integration.jar) 
	UC	ProjRed|Illumination{4.7.0pre12.95} [ProjectRed Illumination] (ProjectRed-1.7.10-4.7.0pre12.95-Lighting.jar) 
	UC	RotaryCraft{1.0} [RotaryCraft] (RotaryCraft 1.7.10 V13a.jar) 
	UC	ReactorCraft{1.0} [ReactorCraft] (ReactorCraft 1.7.10 V13b.jar) 
	UC	RedstoneArsenal{1.7.10R1.1.2} [Redstone Arsenal] (RedstoneArsenal-[1.7.10]1.1.2-92.jar) 
	UC	Tails{1.7.10-1.4.1a} [Tails] (Tails-1.7.10-1.4.1a.jar) 
	UC	ThermalDynamics{1.7.10R1.1.0} [Thermal Dynamics] (ThermalDynamics-[1.7.10]1.1.0-161.jar) 
	UC	Translocator{1.1.2.16} [Translocator] (Translocator-1.7.10-1.1.2.16-universal.jar) 
	UC	WailaHarvestability{1.1.6} [Waila Harvestability] (WailaHarvestability-mc1.7.10-1.1.6.jar) 
	UC	warpbook{2.0.null} [Warp Book] (warpbook-1.7.10_2.0.36.jar) 
	UC	weaponmod{v1.14.3} [Balkon's WeaponMod] (weaponmod-1.14.3.jar) 
	UC	WR-CBE|Core{1.4.1.9} [WR-CBE Core] (WR-CBE-1.7.10-1.4.1.9-universal.jar) 
	UC	WR-CBE|Addons{1.4.1.9} [WR-CBE Addons] (WR-CBE-1.7.10-1.4.1.9-universal.jar) 
	UC	WR-CBE|Logic{1.4.1.9} [WR-CBE Logic] (WR-CBE-1.7.10-1.4.1.9-universal.jar) 
	UC	ForgeMicroblock{1.2.0.347} [Forge Microblocks] (ForgeMultipart-1.7.10-1.2.0.347-universal.jar) 
	UC	McMultipart{1.2.0.347} [Minecraft Multipart Plugin] (ForgeMultipart-1.7.10-1.2.0.347-universal.jar) 
	UC	ForgeRelocation{0.0.1.4} [ForgeRelocation] (ForgeRelocation-1.7.10-0.0.1.4-universal.jar) 
	UC	MCFrames{1.0} [MCFrames] (ForgeRelocation-1.7.10-0.0.1.4-universal.jar) 
	UC	RelocationFMP{0.0.1.2} [RelocationFMP] (ForgeRelocationFMP-1.7.10-0.0.1.2-universal.jar) 
	OpenModsLib class transformers: [stencil_patches:FINISHED],[movement_callback:FINISHED],[map_gen_fix:ENABLED],[gl_capabilities_hook:FINISHED],[player_render_hook:ENABLED]
	Class transformer null safety: all safe
	AE2 Version: stable rv2-stable-10 for Forge 10.13.2.1291
	CoFHCore: -[1.7.10]3.1.2-325
	ThermalFoundation: -[1.7.10]1.2.3-112
	ThermalExpansion: -[1.7.10]4.1.2-240
	RedstoneArsenal: -[1.7.10]1.1.2-92
	ThermalDynamics: -[1.7.10]1.1.0-161
	OptiFine Version: OptiFine_1.7.10_HD_U_D4
	Render Distance Chunks: 10
	Mipmaps: 4
	Anisotropic Filtering: 1
	Antialiasing: 0
	Multitexture: false
	OpenGlVersion: 4.5.13399 Compatibility Profile Context 15.201.1151.1008
	OpenGlRenderer: AMD Radeon HD 6800 Series
	OpenGlVendor: ATI Technologies Inc.
	CpuCount: 4