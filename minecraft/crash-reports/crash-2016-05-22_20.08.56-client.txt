---- Minecraft Crash Report ----
// Daisy, daisy...

Time: 22/05/16 20:08
Description: Rendering entity in world

java.lang.IllegalStateException: Already tesselating!
	at net.minecraft.client.renderer.Tessellator.func_78371_b(Tessellator.java:374)
	at net.minecraft.client.renderer.Tessellator.func_78382_b(Tessellator.java:364)
	at net.minecraft.client.renderer.ItemRenderer.func_78439_a(ItemRenderer.java:172)
	at net.minecraft.client.renderer.entity.RenderItem.renderDroppedItem(RenderItem.java:327)
	at net.minecraft.client.renderer.entity.RenderItem.func_77020_a(RenderItem.java:239)
	at net.minecraft.client.renderer.entity.RenderItem.func_76986_a(RenderItem.java:211)
	at net.minecraft.client.renderer.entity.RenderItem.func_76986_a(RenderItem.java:746)
	at net.minecraft.client.renderer.entity.RenderManager.func_147939_a(RenderManager.java:293)
	at net.minecraft.client.renderer.entity.RenderManager.func_147936_a(RenderManager.java:271)
	at net.minecraft.client.renderer.entity.RenderManager.func_147937_a(RenderManager.java:244)
	at net.minecraft.client.renderer.RenderGlobal.func_147589_a(RenderGlobal.java:716)
	at net.minecraft.client.renderer.EntityRenderer.func_78471_a(EntityRenderer.java:1596)
	at net.minecraft.client.renderer.EntityRenderer.func_78480_b(EntityRenderer.java:1330)
	at net.minecraft.client.Minecraft.func_71411_J(Minecraft.java:1001)
	at net.minecraft.client.Minecraft.func_99999_d(Minecraft.java:898)
	at net.minecraft.client.main.Main.main(SourceFile:148)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at org.multimc.onesix.OneSixLauncher.launchWithMainClass(OneSixLauncher.java:310)
	at org.multimc.onesix.OneSixLauncher.launch(OneSixLauncher.java:395)
	at org.multimc.EntryPoint.listen(EntryPoint.java:170)
	at org.multimc.EntryPoint.main(EntryPoint.java:54)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Stacktrace:
	at net.minecraft.client.renderer.Tessellator.func_78371_b(Tessellator.java:374)
	at net.minecraft.client.renderer.Tessellator.func_78382_b(Tessellator.java:364)
	at net.minecraft.client.renderer.ItemRenderer.func_78439_a(ItemRenderer.java:172)
	at net.minecraft.client.renderer.entity.RenderItem.renderDroppedItem(RenderItem.java:327)
	at net.minecraft.client.renderer.entity.RenderItem.func_77020_a(RenderItem.java:239)
	at net.minecraft.client.renderer.entity.RenderItem.func_76986_a(RenderItem.java:211)
	at net.minecraft.client.renderer.entity.RenderItem.func_76986_a(RenderItem.java:746)
	at net.minecraft.client.renderer.entity.RenderManager.func_147939_a(RenderManager.java:293)

-- Entity being rendered --
Details:
	Entity Type: Item (net.minecraft.entity.item.EntityItem)
	Entity ID: 10722897
	Entity Name: item.tile.markerBlock
	Entity's Exact location: 1034.53, 66.13, 372.75
	Entity's Block location: World: (1034,66,372), Chunk: (at 10,4,4 in 64,23; contains blocks 1024,0,368 to 1039,255,383), Region: (2,0; contains chunks 64,0 to 95,31, blocks 1024,0,0 to 1535,255,511)
	Entity's Momentum: 0.00, -0.00, 0.00

-- Renderer details --
Details:
	Assigned renderer: net.minecraft.client.renderer.entity.RenderItem@621e5e96
	Location: -13.81,-1.50,3.15 - World: (-14,-2,3), Chunk: (at 2,-1,3 in -1,0; contains blocks -16,0,0 to -1,255,15), Region: (-1,0; contains chunks -32,0 to -1,31, blocks -512,0,0 to -1,255,511)
	Rotation: 172.96875
	Delta: 0.95098144
Stacktrace:
	at net.minecraft.client.renderer.entity.RenderManager.func_147936_a(RenderManager.java:271)
	at net.minecraft.client.renderer.entity.RenderManager.func_147937_a(RenderManager.java:244)
	at net.minecraft.client.renderer.RenderGlobal.func_147589_a(RenderGlobal.java:716)
	at net.minecraft.client.renderer.EntityRenderer.func_78471_a(EntityRenderer.java:1596)
	at net.minecraft.client.renderer.EntityRenderer.func_78480_b(EntityRenderer.java:1330)

-- Affected level --
Details:
	Level name: MpServer
	All players: 3 total; [EntityClientPlayerMP['SofiaTrap'/10732368, l='MpServer', x=1048.34, y=67.62, z=369.60], EntityOtherPlayerMP['aerika'/9933284, l='MpServer', x=1046.03, y=66.00, z=405.53], EntityOtherPlayerMP['Tomeno'/10298657, l='MpServer', x=1082.31, y=41.00, z=389.22]]
	Chunk stats: MultiplayerChunkCache: 441, 441
	Level seed: 0
	Level generator: ID 04 - RTG, ver 0. Features enabled: false
	Level generator options: 
	Level spawn location: World: (880,73,464), Chunk: (at 0,4,0 in 55,29; contains blocks 880,0,464 to 895,255,479), Region: (1,0; contains chunks 32,0 to 63,31, blocks 512,0,0 to 1023,255,511)
	Level time: 6849015 game time, 6859156 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: false
	Forced entities: 100 total; [EntityBat['Bat'/10729639, l='MpServer', x=1054.28, y=62.67, z=370.97], EntityItem['item.tile.dirt.default'/10725295, l='MpServer', x=1040.88, y=60.13, z=404.16], EntityClientPlayerMP['SofiaTrap'/10732368, l='MpServer', x=1048.34, y=67.62, z=369.60], EntityItem['item.tile.dirt.default'/10725298, l='MpServer', x=1039.34, y=60.13, z=402.28], EntityItem['item.tile.dirt.default'/10725296, l='MpServer', x=1039.91, y=60.13, z=403.13], EntityItem['item.tile.dirt.default'/10725301, l='MpServer', x=1040.88, y=60.13, z=401.13], EntityItem['item.tile.oreIron'/10728121, l='MpServer', x=1045.88, y=59.13, z=401.13], EntityZombie['Zombie'/10733698, l='MpServer', x=995.50, y=70.00, z=322.50], EntityItem['item.tile.woodSlab.big_oak'/10727808, l='MpServer', x=1045.34, y=59.13, z=401.88], EntityItem['item.tile.dirt.default'/10725259, l='MpServer', x=1039.69, y=60.13, z=404.13], EntityCreeper['Creeper'/10733719, l='MpServer', x=1003.50, y=67.00, z=342.50], EntityBat['Bat'/9887907, l='MpServer', x=1048.25, y=64.10, z=395.94], EntitySkeleton['Skeleton'/10728085, l='MpServer', x=1012.44, y=66.00, z=404.88], EntityItem['item.tile.dirt.default'/10724507, l='MpServer', x=1041.81, y=60.13, z=405.44], EntityItem['item.tile.dirt.default'/10724505, l='MpServer', x=1040.97, y=60.13, z=405.41], EntityItem['item.tile.dirt.default'/10733721, l='MpServer', x=1044.28, y=65.13, z=377.09], EntityItem['item.tile.dirt.default'/10724504, l='MpServer', x=1039.13, y=60.13, z=405.13], EntityCreeper['Creeper'/10733720, l='MpServer', x=1008.38, y=66.00, z=339.56], EntityItem['item.tile.dirt.default'/10726634, l='MpServer', x=1041.81, y=60.13, z=403.81], EntityPig['Pig'/7236903, l='MpServer', x=990.50, y=66.00, z=299.63], EntityCreeper['Creeper'/10723571, l='MpServer', x=994.97, y=64.00, z=332.59], EntityBat['Bat'/10724342, l='MpServer', x=1104.31, y=22.10, z=334.50], EntitySlime['Slime'/10685945, l='MpServer', x=1099.31, y=6.00, z=343.69], EntityItem['item.tile.oreIron'/10728702, l='MpServer', x=1045.97, y=59.13, z=401.91], EntityItem['item.tile.stonebrick'/10731262, l='MpServer', x=1101.88, y=8.13, z=340.19], EntityZombie['Zombie'/10732798, l='MpServer', x=1028.50, y=66.00, z=450.50], EntityBat['Bat'/10547967, l='MpServer', x=1055.53, y=64.10, z=394.97], EntityItem['item.tile.oreIron'/10726908, l='MpServer', x=1045.88, y=59.13, z=402.81], EntitySpider['Spider'/10728897, l='MpServer', x=997.50, y=67.00, z=329.50], EntitySkeleton['Skeleton'/10728903, l='MpServer', x=988.50, y=67.00, z=336.50], EntitySkeleton['Skeleton'/10733511, l='MpServer', x=1122.63, y=70.00, z=397.11], EntitySkeleton['Skeleton'/10733510, l='MpServer', x=1117.59, y=68.00, z=400.34], EntityItem['item.tile.dirt.default'/10726603, l='MpServer', x=1042.13, y=60.13, z=401.81], EntityMechanicalArm['unknown'/106, l='MpServer', x=1046.00, y=66.00, z=373.00], EntityBat['Bat'/10730441, l='MpServer', x=1106.50, y=13.10, z=440.25], EntityBlock['unknown'/107, l='MpServer', x=1034.75, y=70.25, z=382.25], EntitySkeleton['Skeleton'/10733512, l='MpServer', x=1116.50, y=69.00, z=402.50], EntityBlock['unknown'/108, l='MpServer', x=1041.25, y=67.00, z=382.25], EntityBlock['unknown'/109, l='MpServer', x=1041.25, y=70.25, z=373.75], EntityBlock['unknown'/110, l='MpServer', x=1041.40, y=66.00, z=382.40], EntityCreeper['Creeper'/10728915, l='MpServer', x=997.00, y=66.00, z=448.50], EntityOtherPlayerMP['aerika'/9933284, l='MpServer', x=1046.03, y=66.00, z=405.53], EntitySkeleton['Skeleton'/10733527, l='MpServer', x=1008.22, y=66.00, z=353.72], EntitySkeleton['Skeleton'/10733526, l='MpServer', x=1004.50, y=66.00, z=363.50], EntityZombie['Zombie'/10715349, l='MpServer', x=1001.28, y=66.00, z=325.56], EntitySkeleton['Skeleton'/10722782, l='MpServer', x=1008.91, y=66.00, z=404.50], EntitySkeleton['Skeleton'/10733346, l='MpServer', x=1126.94, y=68.00, z=321.50], EntitySkeleton['Skeleton'/10733344, l='MpServer', x=1121.94, y=68.00, z=326.44], EntityZombie['Zombie'/10733099, l='MpServer', x=1123.50, y=68.00, z=378.50], EntityOtherPlayerMP['aerika'/9933284, l='MpServer', x=1046.03, y=66.00, z=405.53], EntityItem['item.tile.dirt.default'/10725417, l='MpServer', x=1039.53, y=60.13, z=400.84], EntityCreeper['Creeper'/10723890, l='MpServer', x=1070.97, y=7.00, z=391.50], EntityPig['Pig'/7236850, l='MpServer', x=998.97, y=66.00, z=328.53], EntityPig['Pig'/7236849, l='MpServer', x=983.38, y=67.00, z=314.06], EntityItem['item.tile.dirt.default'/10724610, l='MpServer', x=1047.88, y=60.13, z=405.88], EntityZombie['Zombie'/10722561, l='MpServer', x=1107.31, y=67.00, z=372.94], EntityPig['Pig'/162, l='MpServer', x=973.50, y=66.00, z=360.66], EntitySkeleton['Skeleton'/10722560, l='MpServer', x=1111.78, y=67.00, z=367.09], EntityCow['Cow'/7236813, l='MpServer', x=1046.47, y=65.00, z=300.31], EntityCreeper['Creeper'/10733568, l='MpServer', x=1071.50, y=68.00, z=448.50], EntityZombie['Zombie'/10727686, l='MpServer', x=1069.50, y=46.00, z=306.50], EntityOtherPlayerMP['Tomeno'/10298657, l='MpServer', x=1082.31, y=41.00, z=389.22], EntityCreeper['Creeper'/10724617, l='MpServer', x=1105.28, y=68.00, z=396.28], EntityPig['Pig'/7743452, l='MpServer', x=1086.50, y=69.00, z=448.22], EntityPig['Pig'/7236805, l='MpServer', x=1093.53, y=68.00, z=324.69], EntityItem['item.tile.dirt.default'/10726162, l='MpServer', x=1042.88, y=60.13, z=400.13], EntitySpider['Spider'/10732311, l='MpServer', x=979.09, y=66.00, z=427.47], EntitySkeleton['Skeleton'/10732314, l='MpServer', x=987.44, y=66.00, z=421.91], EntityOtherPlayerMP['Tomeno'/10298657, l='MpServer', x=1082.31, y=41.00, z=389.22], EntitySpider['Spider'/10732830, l='MpServer', x=977.94, y=67.00, z=321.38], EntityItem['item.tile.dirt.default'/10725731, l='MpServer', x=1023.13, y=63.13, z=363.88], EntitySpider['Spider'/10723170, l='MpServer', x=1112.63, y=67.00, z=339.44], EntityItem['item.tile.dirt.default'/10725729, l='MpServer', x=1024.13, y=64.13, z=364.13], EntityBat['Bat'/10549091, l='MpServer', x=1048.41, y=64.10, z=396.66], EntityItem['item.tile.dirt.default'/10725735, l='MpServer', x=1024.06, y=64.13, z=365.31], EntityItem['item.tile.dirt.default'/10724967, l='MpServer', x=1047.88, y=60.13, z=403.03], EntityItem['item.tile.dirt.default'/10725734, l='MpServer', x=1025.19, y=65.13, z=361.78], EntityPig['Pig'/199, l='MpServer', x=969.53, y=65.00, z=427.50], EntityItem['item.tile.dirt.default'/10725738, l='MpServer', x=1023.66, y=63.13, z=362.19], EntityItem['item.tile.dirt.default'/10725737, l='MpServer', x=1025.19, y=64.13, z=362.13], EntityItem['item.tile.dirt.default'/10725736, l='MpServer', x=1023.28, y=64.13, z=361.72], EntityItem['item.tile.dirt.default'/10725740, l='MpServer', x=1022.34, y=64.13, z=361.63], EntityZombie['Zombie'/10733683, l='MpServer', x=1105.50, y=66.00, z=363.50], EntityZombie['Zombie'/10733682, l='MpServer', x=1103.50, y=66.00, z=363.50], EntitySkeleton['Skeleton'/10733687, l='MpServer', x=1106.50, y=66.00, z=362.50], EntityItem['item.tile.dirt.default'/10732406, l='MpServer', x=1045.88, y=66.13, z=373.19], EntityCreeper['Creeper'/10171724, l='MpServer', x=1063.03, y=63.00, z=403.38], EntityItem['item.tile.ladder'/10731072, l='MpServer', x=1045.13, y=10.13, z=405.88], EntityZombie['Zombie'/10732359, l='MpServer', x=1034.50, y=72.00, z=326.50], EntityCreeper['Creeper'/10733124, l='MpServer', x=1052.50, y=40.00, z=445.50], EntityItem['item.tile.markerBlock'/10722897, l='MpServer', x=1034.53, y=66.13, z=372.75], EntityItem['item.tile.markerBlock'/10722896, l='MpServer', x=1035.56, y=66.13, z=398.34], EntityPig['Pig'/7743618, l='MpServer', x=1093.31, y=68.00, z=326.50], EntityPig['Pig'/7743619, l='MpServer', x=1114.25, y=77.00, z=402.50], EntityPig['Pig'/7743617, l='MpServer', x=1112.09, y=67.00, z=330.84], EntityItem['item.tile.dirt.default'/10725727, l='MpServer', x=1022.13, y=64.13, z=363.78], EntityItem['item.item.bone'/10722142, l='MpServer', x=1040.75, y=60.13, z=401.16], EntityItem['item.tile.dirt.default'/10725725, l='MpServer', x=1024.56, y=63.13, z=362.81], EntityItem['item.item.arrow'/10722141, l='MpServer', x=1041.78, y=60.13, z=400.13], EntityItem['item.tile.dirt.default'/10725724, l='MpServer', x=1046.31, y=67.13, z=374.53]]
	Retry entities: 0 total; []
	Server brand: fml,forge
	Server type: Non-integrated multiplayer server
Stacktrace:
	at net.minecraft.client.Minecraft.func_71396_d(Minecraft.java:2444)
	at net.minecraft.client.Minecraft.func_99999_d(Minecraft.java:919)
	at net.minecraft.client.main.Main.main(SourceFile:148)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at org.multimc.onesix.OneSixLauncher.launchWithMainClass(OneSixLauncher.java:310)
	at org.multimc.onesix.OneSixLauncher.launch(OneSixLauncher.java:395)
	at org.multimc.EntryPoint.listen(EntryPoint.java:170)
	at org.multimc.EntryPoint.main(EntryPoint.java:54)

-- System Details --
Details:
	Minecraft Version: 1.7.10
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_91, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 440248496 bytes (419 MB) / 2032140288 bytes (1938 MB) up to 3817865216 bytes (3641 MB)
	JVM Flags: 3 total; -XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -Xms512m -Xmx4096m
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	IntCache: cache: 0, tcache: 0, allocated: 11, tallocated: 61
	FML: Minecraft 1.7.10 MCP v9.05 FML v7.10.99.99 Minecraft Forge 10.13.4.1614 Optifine OptiFine_1.7.10_HD_U_D4 110 mods loaded, 110 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	UCHIJA	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	UCHIJA	FML{7.10.99.99} [Forge Mod Loader] (forge-1.7.10-10.13.4.1614-1.7.10-universal.jar) 
	UCHIJA	Forge{10.13.4.1614} [Minecraft Forge] (forge-1.7.10-10.13.4.1614-1.7.10-universal.jar) 
	UCHIJA	appliedenergistics2-core{rv2-stable-10} [AppliedEnergistics2 Core] (minecraft.jar) 
	UCHIJA	CodeChickenCore{1.0.7.47} [CodeChicken Core] (minecraft.jar) 
	UCHIJA	Covers1624Core{1.7.10-1.0} [Covers1624 Core] (minecraft.jar) 
	UCHIJA	NotEnoughItems{1.0.5.120} [Not Enough Items] (NotEnoughItems-1.7.10-1.0.5.120-universal.jar) 
	UCHIJA	OpenComputers|Core{1.6.0.3-rc.1} [OpenComputers (Core)] (minecraft.jar) 
	UCHIJA	OpenEye{0.6} [OpenEye] (OpenEye-0.6-1.7.10.jar) 
	UCHIJA	OpenModsCore{0.9.1} [OpenModsCore] (minecraft.jar) 
	UCHIJA	<CoFH ASM>{000} [CoFH ASM] (minecraft.jar) 
	UCHIJA	FastCraft{1.23} [FastCraft] (fastcraft-1.23.jar) 
	UCHIJA	bspkrsCore{6.16} [bspkrsCore] ([1.7.10]bspkrsCore-universal-6.16.jar) 
	UCHIJA	ArmorStatusHUD{1.28} [ArmorStatusHUD] ([1.7.10]ArmorStatusHUD-client-1.28.jar) 
	UCHIJA	StartingInventory{1.7.10.r03} [StartingInventory] ([1.7.10]StartingInventory-universal-1.7.10.r03.jar) 
	UCHIJA	Treecapitator{1.7.10} [Treecapitator] ([1.7.10]Treecapitator-universal-2.0.4.jar) 
	UCHIJA	IC2{2.2.821-experimental} [IndustrialCraft 2] (industrialcraft-2-2.2.821-experimental.jar) 
	UCHIJA	ImmibisCore{59.1.4} [Immibis Core] (immibis-core-59.1.4.jar) 
	UCHIJA	AdvancedRepulsionSystems{59.0.4} [Advanced Repulsion Systems] (adv-repulsion-systems-59.0.4.jar) 
	UCHIJA	AdvancedMachines{1.1.6} [IC2 Advanced Machines Addon] (AdvancedMachinesAS-1.7.10.jar) 
	UCHIJA	appliedenergistics2{rv2-stable-10} [Applied Energistics 2] (appliedenergistics2-rv2-stable-10.jar) 
	UCHIJA	CoFHCore{1.7.10R3.1.2} [CoFH Core] (CoFHCore-[1.7.10]3.1.2-325.jar) 
	UCHIJA	asielib{0.4.5} [asielib] (AsieLib-1.7.10-0.4.5.jar) 
	UCHIJA	BiblioCraft{1.11.4} [BiblioCraft] (BiblioCraft[v1.11.4][MC1.7.10].jar) 
	UCHIJA	Forestry{4.2.12.60} [Forestry for Minecraft] (forestry_1.7.10-4.2.12.60.jar) 
	UCHIJA	BiblioWoodsForestry{1.7} [BiblioWoods Forestry Edition] (BiblioWoods[Forestry][v1.7].jar) 
	UCHIJA	BuildCraft|Core{7.1.16} [BuildCraft] (buildcraft-7.1.16.jar) 
	UCHIJA	BuildCraft|Builders{7.1.16} [BC Builders] (buildcraft-7.1.16.jar) 
	UCHIJA	BuildCraft|Transport{7.1.16} [BC Transport] (buildcraft-7.1.16.jar) 
	UCHIJA	BuildCraft|Energy{7.1.16} [BC Energy] (buildcraft-7.1.16.jar) 
	UCHIJA	BuildCraft|Silicon{7.1.16} [BC Silicon] (buildcraft-7.1.16.jar) 
	UCHIJA	BuildCraft|Robotics{7.1.16} [BC Robotics] (buildcraft-7.1.16.jar) 
	UCHIJA	BuildCraft|Factory{7.1.16} [BC Factory] (buildcraft-7.1.16.jar) 
	UCHIJA	ThermalFoundation{1.7.10R1.2.3} [Thermal Foundation] (ThermalFoundation-[1.7.10]1.2.3-112.jar) 
	UCHIJA	ThermalExpansion{1.7.10R4.1.2} [Thermal Expansion] (ThermalExpansion-[1.7.10]4.1.2-240.jar) 
	UCHIJA	BuildCraft|Compat{7.1.5} [BuildCraft Compat] (buildcraft-compat-7.1.5.jar) 
	UCHIJA	CarpentersBlocks{3.3.7} [Carpenter's Blocks] (Carpenter's Blocks v3.3.7 - MC 1.7.10.jar) 
	UCHIJA	ChickenChunks{1.3.4.19} [ChickenChunks] (ChickenChunks-1.7.10-1.3.4.19-universal.jar) 
	UCHIJA	climatecontrol{0.4} [Climate Control] (ClimateControl-0.5.beta53.jar) 
	UCHIJA	CompactSolars{4.4.41.316} [Compact Solar Arrays] (CompactSolars-1.7.10-4.4.41.316-universal.jar) 
	UCHIJA	EnderStorage{1.4.7.38} [EnderStorage] (EnderStorage-1.7.10-1.4.7.38-universal.jar) 
	UCHIJA	ForgeMultipart{1.2.0.347} [Forge Multipart] (ForgeMultipart-1.7.10-1.2.0.347-universal.jar) 
	UCHIJA	MrTJPCoreMod{1.1.0.33} [MrTJPCore] (MrTJPCore-1.7.10-1.1.0.33-universal.jar) 
	UCHIJA	ProjRed|Core{4.7.0pre12.95} [ProjectRed Core] (ProjectRed-1.7.10-4.7.0pre12.95-Base.jar) 
	UCHIJA	ProjRed|Transmission{4.7.0pre12.95} [ProjectRed Transmission] (ProjectRed-1.7.10-4.7.0pre12.95-Integration.jar) 
	UCHIJA	OpenComputers{1.6.0.3-rc.1} [OpenComputers] (OpenComputers-MC1.7.10-1.6.0.3-rc.1-universal.jar) 
	UCHIJA	Waila{1.5.10} [Waila] (Waila-1.5.10_1.7.10.jar) 
	UCHIJA	computronics{1.6.1} [Computronics] (Computronics-1.7.10-1.6.1-ermahgurd-sine-wavez-OC1.6.jar) 
	UCHIJA	DamageIndicatorsMod{3.2.0} [Damage Indicators] (Damage-Indicators-Mod-1.7.10.jar) 
	UCHIJA	DynamicLights{1.3.9} [Dynamic Lights] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_onFire{1.0.5} [Dynamic Lights Burning Entity Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_creepers{1.0.4} [Dynamic Lights Creeper Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_dropItems{1.0.8} [Dynamic Lights EntityItem Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_entityClasses{1.0.1} [Dynamic Lights Entity Light Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_mobEquipment{1.0.8} [Dynamic Lights Mob Equipment Light Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_flameArrows{1.0.0} [Dynamic Lights Fiery Arrows Light Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_floodLights{1.0.2} [Dynamic Lights Flood Light] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_otherPlayers{1.0.8} [Dynamic Lights OtherPlayers Light Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_thePlayer{1.1.4} [Dynamic Lights Player Light Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	eplus{3.0.2-d} [Enchanting Plus] (EnchantingPlus-1.7.10-3.0.2-d.jar) 
	UCHIJA	enhancedportals{3.0.12} [EnhancedPortals] (EnhancedPortals_1.7.10-universal-3.0.12.jar) 
	UCHIJA	etfuturum{1.5.5} [Et Futurum] (Et Futurum-1.5.5.jar) 
	UCHIJA	rftl{1.7.10} [Rotten Flesh to Leather] (Forge SSP SMP - Rotten Flesh 1.7.2 1.7.10.jar) 
	UCHIJA	foxlib{1.7.10-0.7.0} [FoxLib] (FoxLib-1.7.10-0.7.0.jar) 
	UCHIJA	inventorytweaks{1.59-dev-152-cf6e263} [Inventory Tweaks] (InventoryTweaks-1.59-dev-152.jar) 
	UCHIJA	IronChest{6.0.62.742} [Iron Chest] (ironchest-1.7.10-6.0.60.741-universal.jar) 
	UCHIJA	JABBA{1.2.1} [JABBA] (Jabba-1.2.1a_1.7.10.jar) 
	UCHIJA	journeymap{5.1.4p1} [JourneyMap] (journeymap-1.7.10-5.1.4p1-unlimited.jar) 
	UCHIJA	LogisticsPipes{0.9.3.119} [Logistics Pipes] (logisticspipes-0.9.3.119.jar) 
	UCHIJA	malisiscore{1.7.10-0.14.1} [MalisisCore] (malisiscore-1.7.10-0.14.1.jar) 
	UCHIJA	malisisdoors{1.7.10-1.13.0} [Malisis' Doors] (malisisdoors-1.7.10-1.13.0.jar) 
	UCHIJA	numina{0.4.0.131} [Numina] (Numina-0.4.0.131.jar) 
	UCHIJA	powersuits{0.11.0.300} [MachineMuse's Modular Powersuits] (ModularPowersuits-0.11.0.300.jar) 
	UCHIJA	NEIAddons{1.12.14.40} [NEI Addons] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|Developer{1.12.14.40} [NEI Addons: Developer Tools] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|AppEng{1.12.14.40} [NEI Addons: Applied Energistics 2] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|Botany{1.12.14.40} [NEI Addons: Botany] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|Forestry{1.12.14.40} [NEI Addons: Forestry] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|CraftingTables{1.12.14.40} [NEI Addons: Crafting Tables] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|ExNihilo{1.12.14.40} [NEI Addons: Ex Nihilo] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	neiintegration{1.1.2} [NEI Integration] (NEIIntegration-MC1.7.10-1.1.2.jar) 
	UCHIJA	OpenMods{0.9.1} [OpenMods] (OpenModsLib-1.7.10-0.9.1.jar) 
	UCHIJA	OpenBlocks{1.5.1} [OpenBlocks] (OpenBlocks-1.7.10-1.5.1.jar) 
	UCHIJA	OpenPeripheralCore{1.3} [OpenPeripheralCore] (OpenPeripheral-1.7.10-AIO-7.jar) 
	UCHIJA	OpenPeripheral{0.5.1} [OpenPeripheralAddons] (OpenPeripheral-1.7.10-AIO-7.jar) 
	UCHIJA	OpenPeripheralIntegration{0.5} [OpenPeripheralIntegration] (OpenPeripheral-1.7.10-AIO-7.jar) 
	UCHIJA	ProjRed|Transportation{4.7.0pre12.95} [ProjectRed Transportation] (ProjectRed-1.7.10-4.7.0pre12.95-Mechanical.jar) 
	UCHIJA	ProjRed|Exploration{4.7.0pre12.95} [ProjectRed Exploration] (ProjectRed-1.7.10-4.7.0pre12.95-World.jar) 
	UCHIJA	ProjRed|Compatibility{4.7.0pre12.95} [ProjectRed Compatibility] (ProjectRed-1.7.10-4.7.0pre12.95-Compat.jar) 
	UCHIJA	ProjRed|Integration{4.7.0pre12.95} [ProjectRed Integration] (ProjectRed-1.7.10-4.7.0pre12.95-Integration.jar) 
	UCHIJA	ProjRed|Fabrication{4.7.0pre12.95} [ProjectRed Fabrication] (ProjectRed-1.7.10-4.7.0pre12.95-Fabrication.jar) 
	UCHIJA	ProjRed|Illumination{4.7.0pre12.95} [ProjectRed Illumination] (ProjectRed-1.7.10-4.7.0pre12.95-Lighting.jar) 
	UCHIJA	ProjRed|Expansion{4.7.0pre12.95} [ProjectRed Expansion] (ProjectRed-1.7.10-4.7.0pre12.95-Mechanical.jar) 
	UCHIJA	RedstoneArsenal{1.7.10R1.1.2} [Redstone Arsenal] (RedstoneArsenal-[1.7.10]1.1.2-92.jar) 
	UCHIJA	RTG{0.7.0} [Realistic Terrain Generation] (RTG-1.7.10-0.7.0.jar) 
	UCHIJA	skinport{1.7.10-v8c} [SkinPort] (SkinPort-1.7.10-v8c.jar) 
	UCHIJA	Tails{1.7.10-1.4.1a} [Tails] (Tails-1.7.10-1.4.1a.jar) 
	UCHIJA	ThermalDynamics{1.7.10R1.1.0} [Thermal Dynamics] (ThermalDynamics-[1.7.10]1.1.0-161.jar) 
	UCHIJA	Translocator{1.1.2.16} [Translocator] (Translocator-1.7.10-1.1.2.16-universal.jar) 
	UCHIJA	WailaHarvestability{1.1.6} [Waila Harvestability] (WailaHarvestability-mc1.7.10-1.1.6.jar) 
	UCHIJA	warpbook{2.0.null} [Warp Book] (warpbook-1.7.10_2.0.36.jar) 
	UCHIJA	weaponmod{v1.14.3} [Balkon's WeaponMod] (weaponmod-1.14.3.jar) 
	UCHIJA	WR-CBE|Core{1.4.1.9} [WR-CBE Core] (WR-CBE-1.7.10-1.4.1.9-universal.jar) 
	UCHIJA	WR-CBE|Addons{1.4.1.9} [WR-CBE Addons] (WR-CBE-1.7.10-1.4.1.9-universal.jar) 
	UCHIJA	WR-CBE|Logic{1.4.1.9} [WR-CBE Logic] (WR-CBE-1.7.10-1.4.1.9-universal.jar) 
	UCHIJA	ForgeMicroblock{1.2.0.347} [Forge Microblocks] (ForgeMultipart-1.7.10-1.2.0.347-universal.jar) 
	UCHIJA	McMultipart{1.2.0.347} [Minecraft Multipart Plugin] (ForgeMultipart-1.7.10-1.2.0.347-universal.jar) 
	UCHIJA	ForgeRelocation{0.0.1.4} [ForgeRelocation] (ForgeRelocation-1.7.10-0.0.1.4-universal.jar) 
	UCHIJA	MCFrames{1.0} [MCFrames] (ForgeRelocation-1.7.10-0.0.1.4-universal.jar) 
	UCHIJA	RelocationFMP{0.0.1.2} [RelocationFMP] (ForgeRelocationFMP-1.7.10-0.0.1.2-universal.jar) 
	OpenModsLib class transformers: [stencil_patches:FINISHED],[movement_callback:FINISHED],[map_gen_fix:FINISHED],[gl_capabilities_hook:FINISHED],[player_render_hook:FINISHED]
	Class transformer null safety: all safe
	AE2 Version: stable rv2-stable-10 for Forge 10.13.2.1291
	CoFHCore: -[1.7.10]3.1.2-325
	ThermalFoundation: -[1.7.10]1.2.3-112
	ThermalExpansion: -[1.7.10]4.1.2-240
	RedstoneArsenal: -[1.7.10]1.1.2-92
	ThermalDynamics: -[1.7.10]1.1.0-161
	Stencil buffer state: Function set: GL30, pool: forge, bits: 8
	Forestry : Warning: You have mods that change the behavior of Minecraft, ForgeModLoader, and/or Minecraft Forge to your client: 
Optifine
These may have caused this error, and may not be supported. Try reproducing the crash WITHOUT these mods, and report it then.
	AE2 Integration: IC2:ON, RotaryCraft:OFF, RC:OFF, BC:ON, RF:ON, RFItem:ON, MFR:OFF, DSU:ON, FZ:OFF, FMP:ON, RB:OFF, CLApi:OFF, Waila:ON, InvTweaks:ON, NEI:ON, CraftGuide:OFF, Mekanism:OFF, ImmibisMicroblocks:OFF, BetterStorage:OFF
	Launched Version: MultiMC5
	XRay Version: 15
	LWJGL: 2.9.1
	OpenGL: AMD Radeon HD 6800 Series GL version 4.5.13399 Compatibility Profile Context 15.201.1151.1008, ATI Technologies Inc.
	GL Caps: Using GL 1.3 multitexturing.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Anisotropic filtering is supported and maximum anisotropy is 16.
Shaders are available because OpenGL 2.1 is supported.

	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: []
	Current Language: English (US)
	Profiler Position: N/A (disabled)
	Vec3 Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Anisotropic Filtering: Off (1)
	OptiFine Version: OptiFine_1.7.10_HD_U_D4
	Render Distance Chunks: 10
	Mipmaps: 4
	Anisotropic Filtering: 1
	Antialiasing: 0
	Multitexture: false
	OpenGlVersion: 4.5.13399 Compatibility Profile Context 15.201.1151.1008
	OpenGlRenderer: AMD Radeon HD 6800 Series
	OpenGlVendor: ATI Technologies Inc.
	CpuCount: 4