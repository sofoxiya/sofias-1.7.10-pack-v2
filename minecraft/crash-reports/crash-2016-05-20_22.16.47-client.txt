---- Minecraft Crash Report ----
// Uh... Did I do that?

Time: 20/05/16 22:16
Description: Updating screen events

java.lang.ArrayIndexOutOfBoundsException: 0
	at net.machinemuse.general.gui.frame.PartManipSubFrame.tryMouseClick(PartManipSubFrame.scala:185)
	at net.machinemuse.general.gui.frame.PartManipContainer$$anonfun$onMouseDown$1.apply(PartManipContainer.scala:45)
	at net.machinemuse.general.gui.frame.PartManipContainer$$anonfun$onMouseDown$1.apply(PartManipContainer.scala:44)
	at scala.collection.immutable.List.foreach(List.scala:383)
	at net.machinemuse.general.gui.frame.PartManipContainer.onMouseDown(PartManipContainer.scala:44)
	at net.machinemuse.general.gui.MuseGui.func_73864_a(MuseGui.java:225)
	at net.minecraft.client.gui.GuiScreen.func_146274_d(GuiScreen.java:296)
	at net.minecraft.client.gui.GuiScreen.func_146269_k(GuiScreen.java:268)
	at net.minecraft.client.Minecraft.func_71407_l(Minecraft.java:1640)
	at net.minecraft.client.Minecraft.func_71411_J(Minecraft.java:973)
	at net.minecraft.client.Minecraft.func_99999_d(Minecraft.java:898)
	at net.minecraft.client.main.Main.main(SourceFile:148)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at org.multimc.onesix.OneSixLauncher.launchWithMainClass(OneSixLauncher.java:310)
	at org.multimc.onesix.OneSixLauncher.launch(OneSixLauncher.java:395)
	at org.multimc.EntryPoint.listen(EntryPoint.java:170)
	at org.multimc.EntryPoint.main(EntryPoint.java:54)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Stacktrace:
	at net.machinemuse.general.gui.frame.PartManipSubFrame.tryMouseClick(PartManipSubFrame.scala:185)
	at net.machinemuse.general.gui.frame.PartManipContainer$$anonfun$onMouseDown$1.apply(PartManipContainer.scala:45)
	at net.machinemuse.general.gui.frame.PartManipContainer$$anonfun$onMouseDown$1.apply(PartManipContainer.scala:44)
	at scala.collection.immutable.List.foreach(List.scala:383)
	at net.machinemuse.general.gui.frame.PartManipContainer.onMouseDown(PartManipContainer.scala:44)
	at net.machinemuse.general.gui.MuseGui.func_73864_a(MuseGui.java:225)
	at net.minecraft.client.gui.GuiScreen.func_146274_d(GuiScreen.java:296)
	at net.minecraft.client.gui.GuiScreen.func_146269_k(GuiScreen.java:268)
	at net.minecraft.client.Minecraft.func_71407_l(Minecraft.java:1640)

-- Affected screen --
Details:
	Screen name: net.machinemuse.general.gui.CosmeticGui

-- Affected level --
Details:
	Level name: MpServer
	All players: 1 total; [EntityClientPlayerMP['SofiaTrap'/4073162, l='MpServer', x=1066.60, y=67.62, z=364.82]]
	Chunk stats: MultiplayerChunkCache: 441, 441
	Level seed: 0
	Level generator: ID 04 - RTG, ver 0. Features enabled: false
	Level generator options: 
	Level spawn location: World: (880,73,464), Chunk: (at 0,4,0 in 55,29; contains blocks 880,0,464 to 895,255,479), Region: (1,0; contains chunks 32,0 to 63,31, blocks 512,0,0 to 1023,255,511)
	Level time: 3546345 game time, 3556486 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: survival (ID 0). Hardcore: false. Cheats: false
	Forced entities: 52 total; [EntitySpider['Spider'/4079804, l='MpServer', x=1125.44, y=68.00, z=300.00], EntityZombie['Zombie'/4075829, l='MpServer', x=1056.50, y=11.00, z=355.50], EntityClientPlayerMP['SofiaTrap'/4073162, l='MpServer', x=1066.60, y=67.62, z=364.82], EntityWitch['Witch'/4077088, l='MpServer', x=1141.78, y=31.00, z=287.97], EntityZombie['Zombie'/4075669, l='MpServer', x=1147.69, y=31.00, z=292.31], EntityPig['Pig'/4073234, l='MpServer', x=1141.53, y=72.00, z=437.66], EntityZombie['Zombie'/4074768, l='MpServer', x=1131.50, y=12.00, z=302.50], EntityPig['Pig'/4073233, l='MpServer', x=1142.56, y=79.00, z=421.72], EntitySpider['Spider'/4080140, l='MpServer', x=1092.28, y=64.00, z=414.44], EntityBat['Bat'/4079626, l='MpServer', x=1134.93, y=12.77, z=346.36], EntityPig['Pig'/4073227, l='MpServer', x=993.13, y=65.00, z=291.16], EntityCreeper['Creeper'/4080139, l='MpServer', x=1089.50, y=64.00, z=418.50], EntityBat['Bat'/4079624, l='MpServer', x=1132.10, y=15.81, z=353.27], EntityBat['Bat'/4079625, l='MpServer', x=1134.28, y=10.50, z=338.34], EntityCow['Cow'/4073221, l='MpServer', x=1140.50, y=86.00, z=356.50], EntityCow['Cow'/4073218, l='MpServer', x=1138.25, y=69.00, z=362.41], EntityPig['Pig'/4073216, l='MpServer', x=1131.13, y=79.00, z=314.50], EntityPig['Pig'/4073217, l='MpServer', x=1133.50, y=86.00, z=312.50], EntityPig['Pig'/4073212, l='MpServer', x=994.19, y=66.00, z=307.63], EntityPig['Pig'/4073213, l='MpServer', x=998.13, y=67.00, z=314.59], EntityPig['Pig'/4073210, l='MpServer', x=1131.81, y=68.00, z=333.69], EntitySkeleton['Skeleton'/4077050, l='MpServer', x=1136.50, y=70.00, z=387.50], EntityCreeper['Creeper'/4076406, l='MpServer', x=1124.22, y=68.00, z=298.44], EntitySkeleton['Skeleton'/4073207, l='MpServer', x=1016.50, y=66.00, z=304.50], EntitySkeleton['Skeleton'/4079733, l='MpServer', x=1132.53, y=39.00, z=395.94], EntitySkeleton['Skeleton'/4079731, l='MpServer', x=1129.50, y=39.00, z=398.09], EntityCow['Cow'/4073200, l='MpServer', x=1124.50, y=71.00, z=348.22], EntityZombie['Zombie'/4075889, l='MpServer', x=1135.41, y=11.00, z=344.91], EntityPig['Pig'/4073198, l='MpServer', x=1121.47, y=67.00, z=366.69], EntitySkeleton['Skeleton'/4078830, l='MpServer', x=1137.50, y=73.00, z=389.50], EntityPig['Pig'/4073197, l='MpServer', x=1123.50, y=68.00, z=360.78], EntityWitch['Witch'/4079725, l='MpServer', x=1141.50, y=67.00, z=294.50], EntityZombie['Zombie'/4079978, l='MpServer', x=1127.50, y=16.00, z=356.50], EntityCreeper['Creeper'/4079464, l='MpServer', x=1104.50, y=22.00, z=323.50], EntitySkeleton['Skeleton'/4077030, l='MpServer', x=1149.94, y=13.00, z=341.50], EntityCreeper['Creeper'/4079463, l='MpServer', x=1100.50, y=22.00, z=328.50], EntitySkeleton['Skeleton'/4077029, l='MpServer', x=1144.50, y=13.00, z=337.50], EntityBat['Bat'/4080485, l='MpServer', x=1104.56, y=21.73, z=327.91], EntitySkeleton['Skeleton'/4077027, l='MpServer', x=1146.50, y=13.00, z=339.50], EntityCreeper['Creeper'/4077025, l='MpServer', x=1147.06, y=13.00, z=341.50], EntityBat['Bat'/4073183, l='MpServer', x=1054.78, y=38.10, z=402.34], EntityZombie['Zombie'/4073178, l='MpServer', x=1069.41, y=64.00, z=407.00], EntityZombie['Zombie'/4074454, l='MpServer', x=1140.44, y=31.00, z=286.44], EntityBat['Bat'/4073173, l='MpServer', x=1056.53, y=38.10, z=401.97], EntityCow['Cow'/4073171, l='MpServer', x=1057.38, y=68.00, z=308.31], EntitySlime['Slime'/4073168, l='MpServer', x=1086.31, y=13.00, z=333.69], EntityWitch['Witch'/4073169, l='MpServer', x=1101.06, y=66.00, z=362.53], EntitySlime['Slime'/4073164, l='MpServer', x=1095.31, y=9.09, z=359.69], EntityCreeper['Creeper'/4073163, l='MpServer', x=1042.38, y=79.00, z=379.94], EntitySkeleton['Skeleton'/4080203, l='MpServer', x=1041.50, y=10.00, z=404.50], EntityCreeper['Creeper'/4074825, l='MpServer', x=1118.50, y=20.00, z=366.50], EntityBat['Bat'/4074697, l='MpServer', x=988.31, y=15.10, z=404.00]]
	Retry entities: 0 total; []
	Server brand: fml,forge
	Server type: Non-integrated multiplayer server
Stacktrace:
	at net.minecraft.client.Minecraft.func_71396_d(Minecraft.java:2444)
	at net.minecraft.client.Minecraft.func_99999_d(Minecraft.java:919)
	at net.minecraft.client.main.Main.main(SourceFile:148)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at org.multimc.onesix.OneSixLauncher.launchWithMainClass(OneSixLauncher.java:310)
	at org.multimc.onesix.OneSixLauncher.launch(OneSixLauncher.java:395)
	at org.multimc.EntryPoint.listen(EntryPoint.java:170)
	at org.multimc.EntryPoint.main(EntryPoint.java:54)

-- System Details --
Details:
	Minecraft Version: 1.7.10
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_91, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 590134816 bytes (562 MB) / 1965031424 bytes (1874 MB) up to 3817865216 bytes (3641 MB)
	JVM Flags: 3 total; -XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -Xms512m -Xmx4096m
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	IntCache: cache: 0, tcache: 0, allocated: 11, tallocated: 61
	FML: Minecraft 1.7.10 MCP v9.05 FML v7.10.99.99 Minecraft Forge 10.13.4.1614 Optifine OptiFine_1.7.10_HD_U_D4 110 mods loaded, 110 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	UCHIJA	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	UCHIJA	FML{7.10.99.99} [Forge Mod Loader] (forge-1.7.10-10.13.4.1614-1.7.10-universal.jar) 
	UCHIJA	Forge{10.13.4.1614} [Minecraft Forge] (forge-1.7.10-10.13.4.1614-1.7.10-universal.jar) 
	UCHIJA	appliedenergistics2-core{rv2-stable-10} [AppliedEnergistics2 Core] (minecraft.jar) 
	UCHIJA	CodeChickenCore{1.0.7.47} [CodeChicken Core] (minecraft.jar) 
	UCHIJA	Covers1624Core{1.7.10-1.0} [Covers1624 Core] (minecraft.jar) 
	UCHIJA	NotEnoughItems{1.0.5.120} [Not Enough Items] (NotEnoughItems-1.7.10-1.0.5.120-universal.jar) 
	UCHIJA	OpenComputers|Core{1.6.0.3-rc.1} [OpenComputers (Core)] (minecraft.jar) 
	UCHIJA	OpenEye{0.6} [OpenEye] (OpenEye-0.6-1.7.10.jar) 
	UCHIJA	OpenModsCore{0.9.1} [OpenModsCore] (minecraft.jar) 
	UCHIJA	<CoFH ASM>{000} [CoFH ASM] (minecraft.jar) 
	UCHIJA	FastCraft{1.23} [FastCraft] (fastcraft-1.23.jar) 
	UCHIJA	bspkrsCore{6.16} [bspkrsCore] ([1.7.10]bspkrsCore-universal-6.16.jar) 
	UCHIJA	ArmorStatusHUD{1.28} [ArmorStatusHUD] ([1.7.10]ArmorStatusHUD-client-1.28.jar) 
	UCHIJA	StartingInventory{1.7.10.r03} [StartingInventory] ([1.7.10]StartingInventory-universal-1.7.10.r03.jar) 
	UCHIJA	Treecapitator{1.7.10} [Treecapitator] ([1.7.10]Treecapitator-universal-2.0.4.jar) 
	UCHIJA	IC2{2.2.821-experimental} [IndustrialCraft 2] (industrialcraft-2-2.2.821-experimental.jar) 
	UCHIJA	ImmibisCore{59.1.4} [Immibis Core] (immibis-core-59.1.4.jar) 
	UCHIJA	AdvancedRepulsionSystems{59.0.4} [Advanced Repulsion Systems] (adv-repulsion-systems-59.0.4.jar) 
	UCHIJA	AdvancedMachines{1.1.6} [IC2 Advanced Machines Addon] (AdvancedMachinesAS-1.7.10.jar) 
	UCHIJA	appliedenergistics2{rv2-stable-10} [Applied Energistics 2] (appliedenergistics2-rv2-stable-10.jar) 
	UCHIJA	CoFHCore{1.7.10R3.1.2} [CoFH Core] (CoFHCore-[1.7.10]3.1.2-325.jar) 
	UCHIJA	asielib{0.4.5} [asielib] (AsieLib-1.7.10-0.4.5.jar) 
	UCHIJA	BiblioCraft{1.11.4} [BiblioCraft] (BiblioCraft[v1.11.4][MC1.7.10].jar) 
	UCHIJA	Forestry{4.2.12.60} [Forestry for Minecraft] (forestry_1.7.10-4.2.12.60.jar) 
	UCHIJA	BiblioWoodsForestry{1.7} [BiblioWoods Forestry Edition] (BiblioWoods[Forestry][v1.7].jar) 
	UCHIJA	BuildCraft|Core{7.1.16} [BuildCraft] (buildcraft-7.1.16.jar) 
	UCHIJA	BuildCraft|Builders{7.1.16} [BC Builders] (buildcraft-7.1.16.jar) 
	UCHIJA	BuildCraft|Transport{7.1.16} [BC Transport] (buildcraft-7.1.16.jar) 
	UCHIJA	BuildCraft|Energy{7.1.16} [BC Energy] (buildcraft-7.1.16.jar) 
	UCHIJA	BuildCraft|Silicon{7.1.16} [BC Silicon] (buildcraft-7.1.16.jar) 
	UCHIJA	BuildCraft|Robotics{7.1.16} [BC Robotics] (buildcraft-7.1.16.jar) 
	UCHIJA	BuildCraft|Factory{7.1.16} [BC Factory] (buildcraft-7.1.16.jar) 
	UCHIJA	ThermalFoundation{1.7.10R1.2.3} [Thermal Foundation] (ThermalFoundation-[1.7.10]1.2.3-112.jar) 
	UCHIJA	ThermalExpansion{1.7.10R4.1.2} [Thermal Expansion] (ThermalExpansion-[1.7.10]4.1.2-240.jar) 
	UCHIJA	BuildCraft|Compat{7.1.5} [BuildCraft Compat] (buildcraft-compat-7.1.5.jar) 
	UCHIJA	CarpentersBlocks{3.3.7} [Carpenter's Blocks] (Carpenter's Blocks v3.3.7 - MC 1.7.10.jar) 
	UCHIJA	ChickenChunks{1.3.4.19} [ChickenChunks] (ChickenChunks-1.7.10-1.3.4.19-universal.jar) 
	UCHIJA	climatecontrol{0.4} [Climate Control] (ClimateControl-0.5.beta53.jar) 
	UCHIJA	CompactSolars{4.4.41.316} [Compact Solar Arrays] (CompactSolars-1.7.10-4.4.41.316-universal.jar) 
	UCHIJA	EnderStorage{1.4.7.38} [EnderStorage] (EnderStorage-1.7.10-1.4.7.38-universal.jar) 
	UCHIJA	ForgeMultipart{1.2.0.347} [Forge Multipart] (ForgeMultipart-1.7.10-1.2.0.347-universal.jar) 
	UCHIJA	MrTJPCoreMod{1.1.0.33} [MrTJPCore] (MrTJPCore-1.7.10-1.1.0.33-universal.jar) 
	UCHIJA	ProjRed|Core{4.7.0pre12.95} [ProjectRed Core] (ProjectRed-1.7.10-4.7.0pre12.95-Base.jar) 
	UCHIJA	ProjRed|Transmission{4.7.0pre12.95} [ProjectRed Transmission] (ProjectRed-1.7.10-4.7.0pre12.95-Integration.jar) 
	UCHIJA	OpenComputers{1.6.0.3-rc.1} [OpenComputers] (OpenComputers-MC1.7.10-1.6.0.3-rc.1-universal.jar) 
	UCHIJA	Waila{1.5.10} [Waila] (Waila-1.5.10_1.7.10.jar) 
	UCHIJA	computronics{1.6.1} [Computronics] (Computronics-1.7.10-1.6.1-ermahgurd-sine-wavez-OC1.6.jar) 
	UCHIJA	DamageIndicatorsMod{3.2.0} [Damage Indicators] (Damage-Indicators-Mod-1.7.10.jar) 
	UCHIJA	DynamicLights{1.3.9} [Dynamic Lights] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_onFire{1.0.5} [Dynamic Lights Burning Entity Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_creepers{1.0.4} [Dynamic Lights Creeper Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_dropItems{1.0.8} [Dynamic Lights EntityItem Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_entityClasses{1.0.1} [Dynamic Lights Entity Light Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_mobEquipment{1.0.8} [Dynamic Lights Mob Equipment Light Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_flameArrows{1.0.0} [Dynamic Lights Fiery Arrows Light Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_floodLights{1.0.2} [Dynamic Lights Flood Light] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_otherPlayers{1.0.8} [Dynamic Lights OtherPlayers Light Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	DynamicLights_thePlayer{1.1.4} [Dynamic Lights Player Light Module] (DynamicLights-1.7.10.jar) 
	UCHIJA	eplus{3.0.2-d} [Enchanting Plus] (EnchantingPlus-1.7.10-3.0.2-d.jar) 
	UCHIJA	enhancedportals{3.0.12} [EnhancedPortals] (EnhancedPortals_1.7.10-universal-3.0.12.jar) 
	UCHIJA	etfuturum{1.5.5} [Et Futurum] (Et Futurum-1.5.5.jar) 
	UCHIJA	rftl{1.7.10} [Rotten Flesh to Leather] (Forge SSP SMP - Rotten Flesh 1.7.2 1.7.10.jar) 
	UCHIJA	foxlib{1.7.10-0.7.0} [FoxLib] (FoxLib-1.7.10-0.7.0.jar) 
	UCHIJA	inventorytweaks{1.59-dev-152-cf6e263} [Inventory Tweaks] (InventoryTweaks-1.59-dev-152.jar) 
	UCHIJA	IronChest{6.0.62.742} [Iron Chest] (ironchest-1.7.10-6.0.60.741-universal.jar) 
	UCHIJA	JABBA{1.2.1} [JABBA] (Jabba-1.2.1a_1.7.10.jar) 
	UCHIJA	journeymap{5.1.4p1} [JourneyMap] (journeymap-1.7.10-5.1.4p1-unlimited.jar) 
	UCHIJA	LogisticsPipes{0.9.3.119} [Logistics Pipes] (logisticspipes-0.9.3.119.jar) 
	UCHIJA	malisiscore{1.7.10-0.14.1} [MalisisCore] (malisiscore-1.7.10-0.14.1.jar) 
	UCHIJA	malisisdoors{1.7.10-1.13.0} [Malisis' Doors] (malisisdoors-1.7.10-1.13.0.jar) 
	UCHIJA	numina{0.4.0.131} [Numina] (Numina-0.4.0.131.jar) 
	UCHIJA	powersuits{0.11.0.300} [MachineMuse's Modular Powersuits] (ModularPowersuits-0.11.0.300.jar) 
	UCHIJA	NEIAddons{1.12.14.40} [NEI Addons] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|Developer{1.12.14.40} [NEI Addons: Developer Tools] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|AppEng{1.12.14.40} [NEI Addons: Applied Energistics 2] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|Botany{1.12.14.40} [NEI Addons: Botany] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|Forestry{1.12.14.40} [NEI Addons: Forestry] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|CraftingTables{1.12.14.40} [NEI Addons: Crafting Tables] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	NEIAddons|ExNihilo{1.12.14.40} [NEI Addons: Ex Nihilo] (neiaddons-1.12.14.40-mc1.7.10.jar) 
	UCHIJA	neiintegration{1.1.2} [NEI Integration] (NEIIntegration-MC1.7.10-1.1.2.jar) 
	UCHIJA	OpenMods{0.9.1} [OpenMods] (OpenModsLib-1.7.10-0.9.1.jar) 
	UCHIJA	OpenBlocks{1.5.1} [OpenBlocks] (OpenBlocks-1.7.10-1.5.1.jar) 
	UCHIJA	OpenPeripheralCore{1.3} [OpenPeripheralCore] (OpenPeripheral-1.7.10-AIO-7.jar) 
	UCHIJA	OpenPeripheral{0.5.1} [OpenPeripheralAddons] (OpenPeripheral-1.7.10-AIO-7.jar) 
	UCHIJA	OpenPeripheralIntegration{0.5} [OpenPeripheralIntegration] (OpenPeripheral-1.7.10-AIO-7.jar) 
	UCHIJA	ProjRed|Transportation{4.7.0pre12.95} [ProjectRed Transportation] (ProjectRed-1.7.10-4.7.0pre12.95-Mechanical.jar) 
	UCHIJA	ProjRed|Exploration{4.7.0pre12.95} [ProjectRed Exploration] (ProjectRed-1.7.10-4.7.0pre12.95-World.jar) 
	UCHIJA	ProjRed|Compatibility{4.7.0pre12.95} [ProjectRed Compatibility] (ProjectRed-1.7.10-4.7.0pre12.95-Compat.jar) 
	UCHIJA	ProjRed|Integration{4.7.0pre12.95} [ProjectRed Integration] (ProjectRed-1.7.10-4.7.0pre12.95-Integration.jar) 
	UCHIJA	ProjRed|Fabrication{4.7.0pre12.95} [ProjectRed Fabrication] (ProjectRed-1.7.10-4.7.0pre12.95-Fabrication.jar) 
	UCHIJA	ProjRed|Illumination{4.7.0pre12.95} [ProjectRed Illumination] (ProjectRed-1.7.10-4.7.0pre12.95-Lighting.jar) 
	UCHIJA	ProjRed|Expansion{4.7.0pre12.95} [ProjectRed Expansion] (ProjectRed-1.7.10-4.7.0pre12.95-Mechanical.jar) 
	UCHIJA	RedstoneArsenal{1.7.10R1.1.2} [Redstone Arsenal] (RedstoneArsenal-[1.7.10]1.1.2-92.jar) 
	UCHIJA	RTG{0.7.0} [Realistic Terrain Generation] (RTG-1.7.10-0.7.0.jar) 
	UCHIJA	skinport{1.7.10-v8c} [SkinPort] (SkinPort-1.7.10-v8c.jar) 
	UCHIJA	Tails{1.7.10-1.4.1a} [Tails] (Tails-1.7.10-1.4.1a.jar) 
	UCHIJA	ThermalDynamics{1.7.10R1.1.0} [Thermal Dynamics] (ThermalDynamics-[1.7.10]1.1.0-161.jar) 
	UCHIJA	Translocator{1.1.2.16} [Translocator] (Translocator-1.7.10-1.1.2.16-universal.jar) 
	UCHIJA	WailaHarvestability{1.1.6} [Waila Harvestability] (WailaHarvestability-mc1.7.10-1.1.6.jar) 
	UCHIJA	warpbook{2.0.null} [Warp Book] (warpbook-1.7.10_2.0.36.jar) 
	UCHIJA	weaponmod{v1.14.3} [Balkon's WeaponMod] (weaponmod-1.14.3.jar) 
	UCHIJA	WR-CBE|Core{1.4.1.9} [WR-CBE Core] (WR-CBE-1.7.10-1.4.1.9-universal.jar) 
	UCHIJA	WR-CBE|Addons{1.4.1.9} [WR-CBE Addons] (WR-CBE-1.7.10-1.4.1.9-universal.jar) 
	UCHIJA	WR-CBE|Logic{1.4.1.9} [WR-CBE Logic] (WR-CBE-1.7.10-1.4.1.9-universal.jar) 
	UCHIJA	ForgeMicroblock{1.2.0.347} [Forge Microblocks] (ForgeMultipart-1.7.10-1.2.0.347-universal.jar) 
	UCHIJA	McMultipart{1.2.0.347} [Minecraft Multipart Plugin] (ForgeMultipart-1.7.10-1.2.0.347-universal.jar) 
	UCHIJA	ForgeRelocation{0.0.1.4} [ForgeRelocation] (ForgeRelocation-1.7.10-0.0.1.4-universal.jar) 
	UCHIJA	MCFrames{1.0} [MCFrames] (ForgeRelocation-1.7.10-0.0.1.4-universal.jar) 
	UCHIJA	RelocationFMP{0.0.1.2} [RelocationFMP] (ForgeRelocationFMP-1.7.10-0.0.1.2-universal.jar) 
	OpenModsLib class transformers: [stencil_patches:FINISHED],[movement_callback:FINISHED],[map_gen_fix:FINISHED],[gl_capabilities_hook:FINISHED],[player_render_hook:FINISHED]
	Class transformer null safety: all safe
	AE2 Version: stable rv2-stable-10 for Forge 10.13.2.1291
	CoFHCore: -[1.7.10]3.1.2-325
	ThermalFoundation: -[1.7.10]1.2.3-112
	ThermalExpansion: -[1.7.10]4.1.2-240
	RedstoneArsenal: -[1.7.10]1.1.2-92
	ThermalDynamics: -[1.7.10]1.1.0-161
	Stencil buffer state: Function set: GL30, pool: forge, bits: 8
	Forestry : Warning: You have mods that change the behavior of Minecraft, ForgeModLoader, and/or Minecraft Forge to your client: 
Optifine
These may have caused this error, and may not be supported. Try reproducing the crash WITHOUT these mods, and report it then.
	AE2 Integration: IC2:ON, RotaryCraft:OFF, RC:OFF, BC:ON, RF:ON, RFItem:ON, MFR:OFF, DSU:ON, FZ:OFF, FMP:ON, RB:OFF, CLApi:OFF, Waila:ON, InvTweaks:ON, NEI:ON, CraftGuide:OFF, Mekanism:OFF, ImmibisMicroblocks:OFF, BetterStorage:OFF
	Launched Version: MultiMC5
	XRay Version: 15
	LWJGL: 2.9.1
	OpenGL: AMD Radeon HD 6800 Series GL version 4.5.13399 Compatibility Profile Context 15.201.1151.1008, ATI Technologies Inc.
	GL Caps: Using GL 1.3 multitexturing.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Anisotropic filtering is supported and maximum anisotropy is 16.
Shaders are available because OpenGL 2.1 is supported.

	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: []
	Current Language: English (US)
	Profiler Position: N/A (disabled)
	Vec3 Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Anisotropic Filtering: Off (1)
	OptiFine Version: OptiFine_1.7.10_HD_U_D4
	Render Distance Chunks: 10
	Mipmaps: 4
	Anisotropic Filtering: 1
	Antialiasing: 0
	Multitexture: false
	OpenGlVersion: 4.5.13399 Compatibility Profile Context 15.201.1151.1008
	OpenGlRenderer: AMD Radeon HD 6800 Series
	OpenGlVendor: ATI Technologies Inc.
	CpuCount: 4